#include <cstdlib>
#include <signal.h>
#include "RtAudio.h"
#include "RtMidi.h"

// TODO: read docs to make sure I'm using MY_TYPE correctly
// typedef signed short MY_TYPE;
typedef int16_t MY_TYPE;
#define FORMAT RTAUDIO_SINT16

// Platform-dependent sleep routines.
#if defined( WIN32 )
  #include <windows.h>
  #define SLEEP( milliseconds ) Sleep( (DWORD) milliseconds ) 
#else // Unix variants
  #include <unistd.h>
  #define SLEEP( milliseconds ) usleep( (unsigned long) (milliseconds * 1000.0) )
#endif

#define SAMPLE_RATE 44100
#define TABLESIZE (1 << 8)

bool done;
static void finish( int /*ignore*/ ){ done = true; }

struct Oscillator
{
  int16_t * wavetable;  // pointer to selected wavetable array
  uint32_t counter;  // phase counter
  uint32_t increment;  // phase increment
  uint16_t volume;
  uint32_t start_frame;
  uint32_t stop_frame;
  bool note_held;
};

float scale_clamp(float value, int bits)
{
  float scaled;
  float min;
  float max;

  min = -(1 << (bits - 1));
  max = (1 << (bits - 1)) - 1;
  scaled = value * max;
  scaled = scaled >= max ? max : scaled;
  scaled = scaled < min ? min : scaled;

  return scaled;
}

int16_t interpolate(int16_t a, int16_t b, int16_t interp_value) {
  int16_t interp = a + (int16_t)((b - a) * (interp_value / (1<<17)));
  return interp;
}

void errorCallback( RtAudioError::Type type, const std::string &errorText )
{
  // This example error handling function does exactly the same thing
  // as the embedded RtAudio::error() function.
  std::cout << "in errorCallback" << std::endl;
  if ( type == RtAudioError::WARNING )
    std::cerr << '\n' << errorText << "\n\n";
  else if ( type != RtAudioError::WARNING )
    throw( RtAudioError( errorText, type ) );
}

unsigned int channels;
RtAudio::StreamOptions options;

// Number of frames passed to buffer: used as global clock
uint32_t nFrames = 0;

double envelope(Oscillator* osc_pt)
{
  // Compute scalar to apply to volume to create the ADSR envelope

  uint32_t elapsed_frames;
  double envelope_scalar;
  int ATTACK_PEAK = 2000;
  int DECAY_FLOOR = ATTACK_PEAK + 4000;
  float DECAY_REDUCTION = 0.10;
  float SUSTAIN = 1 - DECAY_REDUCTION;
  float RELEASE_SLOPE = -SUSTAIN / 7000;

  if (osc_pt->note_held) {
      elapsed_frames = nFrames - osc_pt->start_frame;
      if (elapsed_frames < ATTACK_PEAK) {
        // Attack
        envelope_scalar = elapsed_frames / ATTACK_PEAK;
      } else if (elapsed_frames < DECAY_FLOOR) {
        // Decay
        envelope_scalar = 1.0 - DECAY_REDUCTION * (elapsed_frames - ATTACK_PEAK) / (DECAY_FLOOR - ATTACK_PEAK);
      } else {
        // Sustain
        envelope_scalar = SUSTAIN;
      }
  } else {
    // Release
    elapsed_frames = nFrames - osc_pt->stop_frame;
    envelope_scalar = SUSTAIN + RELEASE_SLOPE * elapsed_frames;
  }
  return fmin(fmax(envelope_scalar, 0.0), 1.0);
}

int callback( void *outputBuffer, void * /*inputBuffer*/, unsigned int nBufferFrames,
         double /*streamTime*/, RtAudioStreamStatus status, void *data )
{
  unsigned int i, j;
  extern unsigned int channels;
  MY_TYPE *buffer = (MY_TYPE *) outputBuffer;
  Oscillator *oscillator = (Oscillator *) data;
  uint16_t phase_index;
  uint16_t interp_value;
  int16_t sample;
  uint16_t calc_volume;

  if ( status )
    std::cout << "Stream underflow detected!" << std::endl;

  // TODO: handle multiple channels, but put same values to each

  for ( j=0; j<channels; j++ ) {
    for ( i=0; i<nBufferFrames; i++ ) {
      phase_index = oscillator->counter >> 24;
      interp_value = (oscillator->counter >> 8) & 0xFFFF;
      sample = interpolate(oscillator->wavetable[phase_index], \
                           oscillator->wavetable[phase_index + 1], \
                           interp_value);
      calc_volume = (uint16_t)(oscillator->volume * envelope(oscillator));
      *buffer++ = (MY_TYPE)((sample * calc_volume) >> 16);
      // Increment phase
      oscillator->counter += oscillator->increment;
      nFrames++;
    }
  }

  return 0;
}


int main( int argc, char *argv[] )
{

  // Build wavetables
  static int16_t sine_table[TABLESIZE];
  static int16_t saw_table[TABLESIZE];
  static int16_t tri_table[TABLESIZE];
  static int16_t square_table[TABLESIZE];
  // Fill table
  for (int i = 0; i < TABLESIZE; i++) {
    /* Sine */
    sine_table[i] = (int16_t)scale_clamp(sin((2 * M_PI * i) / TABLESIZE), 16);
    /* Sawtooth */
    saw_table[i] = (int16_t)scale_clamp(-1 + (2.0 * i) / TABLESIZE, 16);
    /* Triangle */
    tri_table[i] = (int16_t)scale_clamp(1 - fabs((4.0 * i) / TABLESIZE - 2), 16);
    /* Square */
    square_table[i] = (int16_t)scale_clamp(i > TABLESIZE / 2 ? 1 : -1, 16);
  }
  // Define oscillator
  int16_t *wave_pt = sine_table;
  Oscillator osc = { wave_pt, 0, 0, 0, 0, 0, false};
  Oscillator *osc_pt = &osc;

  // Other
  float freq = 0;
  int curr_note;

  // Midi declarations
  RtMidiIn *midiin = 0;
  std::vector<unsigned char> message;
  int nBytes;
  double stamp;
  // uint16_t queue_counter = 0;
  // RtMidiIn constructor
  try {
    midiin = new RtMidiIn();
  }
  catch ( RtMidiError &error ) {
    error.printMessage();
    exit( EXIT_FAILURE );
  }
  // Check available ports vs. specified.
  // TODO: Get port from argument instead of defaulting to zero
  unsigned int port = 0;
  unsigned int nPorts = midiin->getPortCount();
  if ( port >= nPorts ) {
    delete midiin;
    std::cout << "Invalid port specifier!\n";
  }

  // Audio declarations
  unsigned int bufferFrames, device = 0, offset = 0;

  RtAudio dac;
  if ( dac.getDeviceCount() < 1 ) {
    std::cout << "\nNo audio devices found!\n";
    exit( 1 );
  }

  // Hardcode number of channels for now
  channels = 1;

  // Let RtAudio print messages to stderr.
  dac.showWarnings( true );

  // Set our stream parameters for output only.
  bufferFrames = 512;
  RtAudio::StreamParameters oParams;
  oParams.deviceId = device;
  oParams.nChannels = channels;
  oParams.firstChannel = offset;

  if ( device == 0 )
    oParams.deviceId = dac.getDefaultOutputDevice();

  /* This HOG flag does not work with Blackhole
  options.flags = RTAUDIO_HOG_DEVICE;
  */
  options.flags |= RTAUDIO_SCHEDULE_REALTIME;
  options.flags |= RTAUDIO_NONINTERLEAVED;

  // Open midi port
  try {
    midiin->openPort( port );
  }
  catch ( RtMidiError &error ) {
    error.printMessage();
    goto cleanup;
  }
  // Warning: not var declaractions can be below the goto statement
  // Don't ignore sysex, timing, or active sensing messages.
  midiin->ignoreTypes( false, false, false );
  // Install an interrupt handler function.
  done = false;
  (void) signal(SIGINT, finish);

  // Start stream
  try {
    dac.openStream( &oParams, NULL, FORMAT, SAMPLE_RATE, &bufferFrames, &callback, (void *)osc_pt, &options, &errorCallback );
    dac.startStream();
  }
  catch ( RtAudioError& e ) {
    e.printMessage();
    goto cleanup;
  }

  // Handle stopping of stream
  std::cout << "Reading MIDI from port " << midiin->getPortName() << " ... quit with Ctrl-C.\n";
  while (!done) {
    // do stuff while stream continues. stopped by sigint ^C.
    stamp = midiin->getMessage( &message );
    nBytes = message.size();
    for ( int i=0; i<nBytes; i++ )
      // Shoe bytes in decimal
      std::cout << "Byte " << i << " = " << (int)message[i] << ", ";
      // Show bytes in hex
      // std::cout << "Byte " << i << " = 0x" << std::hex << (int)message[i] << ", ";
    if ( nBytes > 0 ) {
      std::cout << "stamp = " << stamp << std::endl;

      // Handle midi message
      switch( (int)(message[0] >> 4) ) {
          case 8: // note off
              if (curr_note == (int)message[1]) {
                  osc_pt->note_held = false;
                  osc_pt->stop_frame = nFrames;
              }
              break;
          case 9: // note on
              curr_note = (int)message[1];
              osc_pt->volume = (uint16_t)(message[2]<<8);
              osc_pt->note_held = true;
              osc_pt->start_frame = nFrames;
              // Recompute frequency
              // 12th root of 2 = 1.0594... Note 69 is A4 (440 Hz).
              freq = 440 * pow(1.0594630943593, curr_note - 69);
              // Update phase increment
              /* 1<<32 / 44100 = 97391.55 */
              osc_pt->increment = (uint32_t)(97391.55 * freq + 0.5);
              break;
          case 12:  // set instrument
              // instrument = (int)message[1];
              switch( (int)message[1] ) {
                case 0: osc_pt->wavetable = sine_table; break;
                case 1: osc_pt->wavetable = tri_table; break;
                case 2: osc_pt->wavetable = saw_table; break;
                case 3: osc_pt->wavetable = square_table; break;
                default: osc_pt->wavetable = sine_table; break;
              }
      }
    }
 
    // Sleep for 10 milliseconds.
    SLEEP( 10 );
  }

  // SIGINT received
  try {
    // Stop the stream
    dac.stopStream();
  }
  catch ( RtAudioError& e ) {
    e.printMessage();
  }

 cleanup:
  if ( dac.isStreamOpen() ) dac.closeStream();
  // free( data );
  delete midiin;

  return 0;
}
