#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Code borrowed from: https://python.plainenglish.io/making-a-synth-with-python-oscillators-2cb8e68e9c3b"""

from os import path
import matplotlib.pyplot as plt
from scipy.io import wavfile
import librosa
import numpy as np

SR = 44100
figsize = (25, 6.25)
colors = "#323031", "#308E91", "#34369D", "#5E2A7E", "#5E2A7E", "#6F3384"


def wave_to_file(wav, wav2=None, fname="temp.wav", amp=0.1, sample_rate=44100):
    wav = np.array(wav)
    wav = np.int16(wav * amp * (2 ** 15 - 1))

    if wav2 is not None:
        wav2 = np.array(wav2)
        wav2 = np.int16(wav2 * amp * (2 ** 15 - 1))
        wav = np.stack([wav, wav2]).T

    wavfile.write(fname, sample_rate, wav)


def fplot_xy(wave, fslice=slice(0, 100), sample_rate=SR):
    fd = np.fft.fft(wave)
    fd_mag = np.abs(fd)
    x = np.linspace(0, sample_rate, len(wave))
    y = fd_mag * 2 / sample_rate
    return x[fslice], y[fslice]


sine = wavfile.read("../output/osc_sine.wav")
tri = wavfile.read("../output/osc_triangle.wav")
saw = wavfile.read("../output/osc_sawtooth.wav")
square = wavfile.read("../output/osc_square.wav")
whistle = wavfile.read("../output/whistle2.wav")
# Make mono
whistle = (whistle[0],whistle[1][:,0])

# Problems:
# 1. Triangle: very square; low granularity. Formula may be wrong.
# The odd-number harmonics are a big too strong, too much like square wave.
# 2. Sawtooth: has MAX of 0. Also very square. Almost identical to square wave.
# It does not have the even-numbered harmonics like it's supposed to.

# Is the precision in my calculation being lost?
# Or is the order of operations wrong?
# Is the sine formula using a float and the others an int?

waves = {"sine": sine, "triangle": tri, "sawtooth": saw, "square": square,
         "whistle": whistle}
for name, wave in waves.items():
    print(name)
    print(f"Sample rate: {wave[0]}")
    print(f"Shape: {wave[1].shape}")
    print(f"Min: {wave[1].min():.2f}")
    print(f"Mean: {wave[1].mean():.2f}")
    print(f"Max: {wave[1].max():.2f}")

# Waveform plots
LENGTH = int(4 * SR / 440)
for name, wave in waves.items():
    fig, ax = plt.subplots()
    ax.plot(wave[1][:LENGTH])
    ax.set_title(name)
LENGTH = 20
for name, wave in waves.items():
    fig, ax = plt.subplots()
    ax.plot(wave[1][:LENGTH], marker='o')
    ax.set_title(name)

# Frequency plots
fslice = slice(80, 440 * 5 + 100)
# fig = plt.figure(figsize=figsize)
fig, ax = plt.subplots(1, 4, figsize=figsize)
fig.xlabel = "Hz"
fig.ylabel = "Energy"

for idx, (name, wave) in enumerate(waves.items()):
    x, y = fplot_xy(wave[1], fslice)
    ax[idx].plot(x, y, color=colors[idx])
    ax[idx].set_title(name)
    ax[idx].set_ylim([0, 20000])

fig.savefig("../output/freq_plot.png")


# Frequency plots
# fslice = slice(80, 400)
fig, ax = plt.subplots(figsize=figsize)
fig.xlabel = "Hz"
fig.ylabel = "Energy"
# x, y = fplot_xy(whistle[1], fslice)
x, y = fplot_xy(whistle[1])
ax.plot(x, y, color=colors[0])
ax.set_title('whistle')
# ax.set_ylim([0, 20000])
fig.savefig("../output/whistle_plot.png")
