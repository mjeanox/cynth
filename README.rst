cynth
=====

This is a toy synth written in C++.
It uses the `RtMidi <https://www.music.mcgill.ca/~gary/rtmidi/>`__ library to handle MIDI input
and the `RtAudio <https://www.music.mcgill.ca/~gary/rtaudio/>`__ library to output audio to hardware speakers.

Usage
-----

To compile:

.. code:: bash

    make synth

To start synth:

.. code:: bash

    ./synth

The waveform type is changed using the "patch change" MIDI message:
``0xC0`` for sine, ``0xC1`` for triangle, ``0xC2`` for saw, and ``0xC3`` for square.

Caveats
-------

*   This will only compile properly on MacOS.
*   The program expects MIDI inputs to come from MIDI port 0. It does not scan the ports for a device.
*   The synth is monophonic.
*   The sound enevelope is pretty crude so it doesn't sound great.
*   There is probably aliasing when higher frequencies are played.
