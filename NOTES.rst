
TODO
----

*   Fix the C headers: use C++ headers? Rename?
*   Add more code to handle midi events; or use a library?
*   Try different granularities in sine wave table and phase increment
*   Increase size of wavetable to 512 or 1024?
*   Add polyphony: Sum the waves from the voices for the sample output.
*   Harmonize the type of the index with the size of the wavetable?
    E.g., with an 8-bit (256-element) wavetable, should use ``uint8_t`` for index?
*   Better monophony: keep track of a stack of active keys (keys with no NOTEOFF yet)
    and play the previous active key when the current one gets a NOTEOFF

Done
````

*   Add enevelop to waveform
*   Preset patches which change by midi message?
    Use midi event to switch between waveform types?
*   Better monophony:
    Keep track of note of the one voice.
    Only set volume to zero when NOTE_OFF event is for the current note.
*   Add volume to oscillator struct
    Add volume/amplitude variable when computing waveform.
    Set volume to zero when no notes played.
*   Create Oscillator object with parameters: frequency, amplitude/volume, phase
    [Used a ``struct`` instead.]
*   Add library to play sounds directly (JUCE? ADL?) [RtAudio]
*   Add clock to time the sample output rate and the checking of the queue??
    See: https://github.com/OneLoneCoder/synth/blob/master/main4.cpp
    and: https://stackoverflow.com/questions/48448491/c-precise-44100hz-clock-for-real-time-audio-synthesis
    which use ``std::chrono::high_resolution_clock``
*   Write Python code to plot (like an oscilloscope) to validate wave definition code 
*   Write Python code to plot frequencies present in a wave using spectral stuff.
*   Use spectral analysis to check for aliasing and harmonics present
*   Add sawtooth oscillator
*   Make freq an argument
*   Use incrementer technique from `Roto <https://raw.githubusercontent.com/pteichman/roto/master/Soul%20from%20Scratch.pdf>`__.
    See `increment calc <https://github.com/pteichman/roto/blob/master/tonewheel_osc.cpp#L122>`__.

Won't Do
````````

*   Add lookup table for frequencies of midi pitches. (Is there one in the roto project I can copy?)

Suggestions
-----------

.. code::

    ZZ: something you might want to do is generate higher octave versions at a very high sampling rate, and then filter to nyquist, and then select from them based on the pitch of the note to avoid aliasing
    MJ: What do you mean by 'filter to nyquist'? Do you mean to filter frequencies above the Nyquist limit?
    ZZ: yeah
    MJ: I sorta understand that you need bigger/higher-resolution wavetables to generate higher frequencies. I don't really understand how the size of the wavetable interacts with the sampling rate and aliasing.
    ZZ: basically make each use half the samples as the previous but cover the same space
    ZZ: https://en.wikipedia.org/wiki/Nyquist%E2%80%93Shannon_sampling_theorem
    ZZ: read this
    MJ: Okay, I'll check that out, thanks
    MJ: One other question: are you saying that the wavetables for generating higher freq pitches should use half the samples as the ones for the lower freq pitches?
    MJ: Or it is the other way around?
    ZZ: yes
    ZZ: basically the higher pitch it is, the fewer harmonics there are before you hit nyquist
    MJ: Oh, so by having a smaller wavetable, you generate fewer harmonics?
    ZZ: yeah
    MJ: Ah, cool. That makes sense.
    ZZ: but you need to generate it in a band-limited way
    ZZ: in other words, upsample at a multiple of the original sample rate (maybe 4 + the number of octaves as a multiplier), filter it at nyquist with a high order filter (i.e. very high db per octave, you can stack lower order filters to get a higher order one), and then reduce the sampling rate
    MJ: And the sample rate you reduce to will be lower than the original sample rate?
    MJ: And I guess to do the filtering, you have to do some kind of Fourier transform, filter, and then transform back to a wave
    ZZ: yeah an fft
    MJ: Okay, cool, thanks for your help!

Playing streams with `sox`
--------------------------

Install ``sox``:

.. code:: bash

    brew install sox

Play the stream:

.. code:: bash

    ./exe/sine_example | play -r 44100 -e signed -b 16 -L -t raw -

Write the stream to a ``.wav``:

.. code:: bash

    ./exe/sine_example | sox -r 44100 -e signed -b 16 -L -t raw - output/sine.wav

``sox`` can be installed using Homebrew:

Shorten the ``.wav``:

.. code:: bash

    sox output/sine.wav output/short.wav trim [seconds]

Docs
----

`RtMidi docs <https://www.music.mcgill.ca/~gary/rtmidi/>`__

Resources
---------

`Roto <https://github.com/pteichman/roto>`__

`MusicDSP.org <https://www.musicdsp.org/en/latest/index.html>`__

`Awesome Audio DSP <https://github.com/BillyDM/Awesome-Audio-DSP>`__

`Example of sine wave synthesis <https://old.reddit.com/r/synthesizers/comments/7rdvxl/how_to_get_into_designing_my_own_digital_hardware/dswyrzt/>`__

`Python synth <https://python.plainenglish.io/making-a-synth-with-python-oscillators-2cb8e68e9c3b?gi=2d6a0e52e0d7>`__
